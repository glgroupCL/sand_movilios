import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'faena',
    loadChildren: () => import('./faena/faena.module').then( m => m.FaenaPageModule)
  },
  {
    path: 'inicio',
    loadChildren: () => import('./inicio/inicio.module').then( m => m.InicioPageModule)
  },
  {
    path: 'usuario',
    loadChildren: () => import('./usuario/usuario.module').then( m => m.UsuarioPageModule)
  },
  {
    path: 'manuales',
    loadChildren: () => import('./manuales/manuales.module').then( m => m.ManualesPageModule)
  },
  {
    path: 'flotaactiva',
    loadChildren: () => import('./flotaactiva/flotaactiva.module').then( m => m.FlotaactivaPageModule)
  },
  {
    path: 'flotainactiva',
    loadChildren: () => import('./flotainactiva/flotainactiva.module').then( m => m.FlotainactivaPageModule)
  },
  {
    path: 'flotaactivadetalle',
    loadChildren: () => import('./flotaactivadetalle/flotaactivadetalle.module').then( m => m.FlotaactivadetallePageModule)
  },
  {
    path: 'flotainactivadetalle',
    loadChildren: () => import('./flotainactivadetalle/flotainactivadetalle.module').then( m => m.FlotainactivadetallePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
