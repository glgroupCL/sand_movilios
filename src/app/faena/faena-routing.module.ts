import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FaenaPage } from './faena.page';

const routes: Routes = [
  {
    path: '',
    component: FaenaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FaenaPageRoutingModule {}
