import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FaenaPageRoutingModule } from './faena-routing.module';

import { FaenaPage } from './faena.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FaenaPageRoutingModule
  ],
  declarations: [FaenaPage]
})
export class FaenaPageModule {}
