import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FaenaPage } from './faena.page';

describe('FaenaPage', () => {
  let component: FaenaPage;
  let fixture: ComponentFixture<FaenaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaenaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FaenaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
