import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FaenaService } from './faena.service';
import { Storage } from '@ionic/storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import {LoadingController, NavController} from '@ionic/angular';

@Component({
  selector: 'app-faena',
  templateUrl: './faena.page.html',
  styleUrls: ['./faena.page.scss'],
})
export class FaenaPage implements OnInit {

  public faenas = [

  ];

  constructor(public loadingController: LoadingController, public screenOrientation: ScreenOrientation,
              private navCtrl: NavController,
              private faenaService: FaenaService, private router: Router, private activatedRoute: ActivatedRoute,
              private storage: Storage) { }

  ngOnInit() {
    this.loadingController.create({
      message: 'Cargando...',
      spinner: 'bubbles'
    }).then((loading) => {
      loading.present();
      this.faenaService.getFaenas()
      .subscribe(data => {
        data.datos.resultado.forEach((element, j) => {
          let nombre = element.descripcion, name = "";
          for(let i = 0; i < nombre.length; ++i) {
            if(nombre[i] == ' ') {
              name += '+';
            } else {
              name += nombre[i];
            }
          }
          data.datos.resultado[j].imageUrl = 'https://ui-avatars.com/api/?name=' + name +'&background=2443B7&color=fff';

        });
        this.faenas = data.datos.resultado;
        this.faenaService.setFaenas(this.faenas);
        loading.dismiss();
      }, error => {
        console.log("Some error!");
        this.faenas = error.error.datos.resultado;
      });
    })
  }
  ionViewWillEnter() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.storage.get("logged").then(data => {
      if(data != true) {
        this.router.navigate(['/']);
      } else {
        this.storage.set("onfaena", true);
      } 
    })
  }
  IrInicio(idFaena, faenanombre) {
    this.storage.set("idFaena", idFaena);
    this.storage.set("faenanombre", faenanombre);
    this.navCtrl.navigateRoot(["/inicio"]);
  }
}
