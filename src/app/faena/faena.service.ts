import { Injectable } from '@angular/core';
import { HttpServiciosService } from '../http-servicios.service';

@Injectable({
  providedIn: 'root'
})
export class FaenaService {

  faenas = [];

  constructor(public httpServicios: HttpServiciosService) { }
  
  getFaenas() {
    return this.httpServicios.httpListarFaena()
  }
  setFaenas(fs) {
    this.faenas = fs;
  }
}
