import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FlotaactivaPage } from './flotaactiva.page';

const routes: Routes = [
  {
    path: '',
    component: FlotaactivaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlotaactivaPageRoutingModule {}
