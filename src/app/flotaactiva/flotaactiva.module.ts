import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FlotaactivaPageRoutingModule } from './flotaactiva-routing.module';

import { FlotaactivaPage } from './flotaactiva.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlotaactivaPageRoutingModule
  ],
  declarations: [FlotaactivaPage]
})
export class FlotaactivaPageModule {}
