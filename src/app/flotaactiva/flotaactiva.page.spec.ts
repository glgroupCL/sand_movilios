import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FlotaactivaPage } from './flotaactiva.page';

describe('FlotaactivaPage', () => {
  let component: FlotaactivaPage;
  let fixture: ComponentFixture<FlotaactivaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlotaactivaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FlotaactivaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
