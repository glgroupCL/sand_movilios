import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FlotaactivadetallePage } from './flotaactivadetalle.page';

const routes: Routes = [
  {
    path: '',
    component: FlotaactivadetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlotaactivadetallePageRoutingModule {}
