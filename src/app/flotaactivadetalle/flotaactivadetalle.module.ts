import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FlotaactivadetallePageRoutingModule } from './flotaactivadetalle-routing.module';

import { FlotaactivadetallePage } from './flotaactivadetalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlotaactivadetallePageRoutingModule
  ],
  declarations: [FlotaactivadetallePage]
})
export class FlotaactivadetallePageModule {}
