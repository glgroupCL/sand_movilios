import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FlotaactivadetallePage } from './flotaactivadetalle.page';

describe('FlotaactivadetallePage', () => {
  let component: FlotaactivadetallePage;
  let fixture: ComponentFixture<FlotaactivadetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlotaactivadetallePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FlotaactivadetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
