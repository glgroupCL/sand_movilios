import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FlotainactivaPage } from './flotainactiva.page';

const routes: Routes = [
  {
    path: '',
    component: FlotainactivaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlotainactivaPageRoutingModule {}
