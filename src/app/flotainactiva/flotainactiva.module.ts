import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FlotainactivaPageRoutingModule } from './flotainactiva-routing.module';

import { FlotainactivaPage } from './flotainactiva.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlotainactivaPageRoutingModule
  ],
  declarations: [FlotainactivaPage]
})
export class FlotainactivaPageModule {}
