import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FlotainactivaPage } from './flotainactiva.page';

describe('FlotainactivaPage', () => {
  let component: FlotainactivaPage;
  let fixture: ComponentFixture<FlotainactivaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlotainactivaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FlotainactivaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
