import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { HttpServiciosService } from '../http-servicios.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-flotainactiva',
  templateUrl: './flotainactiva.page.html',
  styleUrls: ['./flotainactiva.page.scss'],
})
export class FlotainactivaPage implements OnInit {

  public componenteS = "TODOS";
  public componentes = [];
  public componenteSelect = [];
  public fechaInicio = "2000-01-01";
  public fechaFin = "2021-01-12";
  public busqueda = "";

  constructor(public loadingController: LoadingController,public screenOrientation: ScreenOrientation, private storage: Storage, private activatedRoute: ActivatedRoute, private router: Router, private http: HttpServiciosService) { }

  ngOnInit() {
    this.loadingController.create({
      message: "Cargando...",
      spinner: "bubbles"
    }).then((loading) => {
      loading.present();
      this.getComponentes(loading);
    });
  }
  ionViewWillEnter() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.storage.get("logged").then(data => {
      if(data != true) {
        this.router.navigate(['/']);
      } 
    })
  }
  getComponentes(loading) {
    this.componenteSelect = [];
    let fi = "", ff = "";
    for(let i = 0; i < 10; ++i) {
      fi += this.fechaInicio[i];
      ff += this.fechaFin[i];
    }
    this.fechaInicio = fi;
    this.fechaFin = ff;
    this.storage.get("idFaena").then(data => {
      this.http.httpGetComponentes(0, this.fechaInicio, this.fechaFin, data).subscribe(data => {
        this.componentes = data.datos.resultado;
        console.log(this.componentes);
        for(let i = 0; i < this.componentes.length; ++i) {
          if(this.componentes[i].ubicacionImagen == null) {
            this.componentes[i].ubicacionImagen = "https://centurycloud.net/Resources/img/sinFoto.jpg";
          } else {
            this.componentes[i].ubicacionImagen = "http://138.99.6.164:9000" + this.componentes[i].ubicacionImagen;
          }
          let sw = false;
          for(let j = 0; j < this.componenteSelect.length; ++j) {
            if(this.componenteSelect[j] == this.componentes[i].nombreComponente) {
              sw = true;
              break;
            }
          }
          if(!sw) {
            this.componenteSelect.push(this.componentes[i].nombreComponente);
          }
        }
        loading.dismiss();
      });
    });
  }
  IrComponenteDetalle(componenteid, componentenombre) {
    this.storage.set("componenteid", componenteid);
    this.storage.set("componentenombre", componentenombre);
    this.router.navigate(["/flotainactivadetalle"]);
  }
}
