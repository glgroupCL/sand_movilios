import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FlotainactivadetallePage } from './flotainactivadetalle.page';

const routes: Routes = [
  {
    path: '',
    component: FlotainactivadetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlotainactivadetallePageRoutingModule {}
