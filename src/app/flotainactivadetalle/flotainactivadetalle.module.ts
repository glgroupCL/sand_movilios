import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FlotainactivadetallePageRoutingModule } from './flotainactivadetalle-routing.module';

import { FlotainactivadetallePage } from './flotainactivadetalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlotainactivadetallePageRoutingModule
  ],
  declarations: [FlotainactivadetallePage]
})
export class FlotainactivadetallePageModule {}
