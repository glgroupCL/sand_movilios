import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FlotainactivadetallePage } from './flotainactivadetalle.page';

describe('FlotainactivadetallePage', () => {
  let component: FlotainactivadetallePage;
  let fixture: ComponentFixture<FlotainactivadetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlotainactivadetallePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FlotainactivadetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
