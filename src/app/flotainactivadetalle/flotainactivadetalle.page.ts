import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { isBuffer } from 'util';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { HttpServiciosService } from '../http-servicios.service';
import { LoadingController } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx'

@Component({
  selector: 'app-flotainactivadetalle',
  templateUrl: './flotainactivadetalle.page.html',
  styleUrls: ['./flotainactivadetalle.page.scss'],
})
export class FlotainactivadetallePage implements OnInit {

  public componenteid: any;
  public componentenombre: any;
  public componente = {
    id_componente: "",
    nombre_componente: "",
    idMotivo: 0,
    rutaDocumento: "",
    t_horaCarga: 0,
    nombreDocumento: "",
    fechaDesactivacion: null
  };
  public documentos = [
    {
      ubicacion: null,
      nombre: null
    },
    {
      ubicacion: null,
      nombre: null
    },
    {
      ubicacion: null,
      nombre: null
    },
    {
      ubicacion: null,
      nombre: null
    }
  ];
  public estado = "#2dd36f";
  public porcentaje = 0;
  constructor(private transfer: FileTransfer, private file: File, public loadingController:LoadingController ,public screenOrientation: ScreenOrientation, private storage: Storage, private activatedRoute: ActivatedRoute, private router: Router, private http: HttpServiciosService) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.loadingController.create({
      message: "Cargando...",
      spinner: "bubbles"
    }).then((loading) => {

      
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.storage.get("logged").then(data => {
        if (data !== true) {
          this.router.navigate(['/']);
        }
      });
      this.storage.get("componenteid").then(data => {
        this.componenteid = data;
        this.storage.get("componentenombre").then(dato => {
          this.componentenombre = dato;
        })
        this.http.httpGetComponenteDesactivado(data).subscribe(res => {
          this.componente = res.datos.resultado[0];
          let a = this.componente.t_horaCarga;
          this.porcentaje = a/100000;
          if(a <= 25000) {
            this.estado = "#2dd36f"
          } else if(a <= 50000) {
            this.estado = "#e8ff1c";
          } else if(a <= 75000) {
            this.estado = "#ffc409";
          } else {
            this.estado = "#eb445a";
          }
          this.documentos[this.componente.idMotivo - 1].ubicacion = this.componente.rutaDocumento;
          this.documentos[this.componente.idMotivo - 1].nombre = this.componente.nombreDocumento;
          loading.dismiss();
        }, error => {
          loading.dismiss();
          console.log('Some error!');
          this.http.toast('Verifique su conexion a internet');
        });
      });
    });
  }
  descargar(ubicacion: string, nombre: string) {
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log(ubicacion + " " + nombre);
    this.loadingController.create({
      message: 'Descargando...',
      spinner: 'bubbles'
    }).then((loading) => {
      loading.present();
      this.http.httpDescargar(ubicacion, nombre).subscribe(response => {
        loading.dismiss();
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
        /*if (nombre)
            downloadLink.setAttribute('download', nombre);
        document.body.appendChild(downloadLink);
        console.log(downloadLink.href);
        downloadLink.click();*/
        fileTransfer.download(downloadLink.href, this.file.dataDirectory + '/' + nombre).then((entry) => {
          console.log('download complete: ' + entry.toURL());
        }, (error) => {
          console.log('error');
        });
      }, error => {
        loading.dismiss();
        console.log('Some error!');
        this.http.toast('Verifique su conexion a internet');
      });

    });
  }

}
