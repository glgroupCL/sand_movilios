import { Component } from '@angular/core';
import { LoginService } from './login.service';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public usuario: string = "";
  public contrasena: string = "";
  public recordar = true;
  public ojo: string = "eye-off-outline";
  public passwordtype: string = "password";

  credenciales = {};

  constructor(public screenOrientation: ScreenOrientation, private loginService: LoginService, private storage: Storage, private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    
  }
  ionViewWillEnter() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.credenciales = this.loginService.getCredenciales();
    this.storage.get("onfaena").then(ans => {
      this.storage.get("logged").then(data => {
        if(data == true && ans != true) {
          this.router.navigate(['/inicio']);
        } else {
          this.storage.get("usuariog").then(dato => {
            this.usuario = dato;
            this.storage.get("contrasenag").then(res => {
              this.contrasena = res;
            });
          });
        }
      });
    })
  }

  Ingresar() {
    if(this.recordar == true) {
      this.storage.set("usuariog", this.usuario);
      this.storage.set("contrasenag", this.contrasena);
    } else {
      this.storage.set("usuariog", null);
      this.storage.set("contrasenag", null);
    }
    console.log("Ingresando...");
    this.credenciales = this.loginService.ingresar(this.usuario, this.contrasena);
  }
  toggleEye() {
    if(this.ojo == "eye-outline") {
      this.ojo = "eye-off-outline";
      this.passwordtype = "password";
    } else {
      this.ojo = "eye-outline";
      this.passwordtype = "text";
    }
  }  
}
