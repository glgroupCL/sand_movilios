import { Injectable } from '@angular/core';
import { Md5 } from 'ts-md5/dist/md5';
import { HttpServiciosService } from '../http-servicios.service';
import {LoadingController} from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private credenciales = {
  }

  constructor(public loadingController: LoadingController, public httpServicios: HttpServiciosService) { }

  async ingresar(usuario: string, pass: string) {
    this.credenciales = {
      usuario: usuario,
      pass: Md5.hashStr(pass)
    }
    this.loadingController.create({
      message: 'Ingresando...',
      spinner: 'bubbles'
    }).then(async (loading) => {
      loading.present();
      console.log("en el servicio...");
      this.credenciales = await this.httpServicios.httpAutenticaUsuario(this.credenciales);
      loading.dismiss();
      return this.credenciales;
    })
  }

  getCredenciales() {
    return this.credenciales;
  }
}
