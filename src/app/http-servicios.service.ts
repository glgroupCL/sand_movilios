import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AlertController, NavController, ToastController} from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class HttpServiciosService {

  private headers = new HttpHeaders();

  constructor(public http: HttpClient, private AlertCtrl: AlertController,
              public toastCtrl: ToastController, public navCtrl: NavController, private activatedRoute: ActivatedRoute,
              private router: Router,
              private storage: Storage
    ) {
    this.headers.append("Accept", 'application/json');
    this.headers.append('Content-Type', 'application/json' );
  }

  async toast(mensaje: string, duracion?: number) {
    const mToast = await this.toastCtrl.create({
      message: mensaje,
      duration: duracion,
      buttons: [
        {
          text: 'Cerrar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    mToast.present();
  }
  async httpAutenticaUsuario(datos) {
    console.log("ahora sip");
    return this.http.post("http://138.99.6.164:8099/AutenticaUsuario", datos, {headers: this.headers})
    .subscribe(dato => {
      console.log("words!");
      let data = {
        datos: {
          resultado: null
        }
      };
      data = <any>dato;
      this.storage.set('token', data.datos.resultado.token);
      this.storage.set('logged', true);
      this.storage.set('idusuario', data.datos.resultado.id);
      this.storage.set('perfilusuario', data.datos.resultado.perfilusuario);
      console.log(data);
      if (data.datos.resultado.id_faena === 0){
        this.navCtrl.navigateRoot(['/faena']);
        this.storage.set('administrador', true);
        return data.datos.resultado;
      }else{
        this.storage.set('administrador', false);
        this.loginFaena(data.datos.resultado.id_faena);
        // this.loginFaena(1);
      }
     }, async error => {
       // algun error
       console.log('error');
       console.log(error);
        const alertElement = await this.AlertCtrl.create({
        header: 'Intente de nuevo',
        message: error.error.datos.mensaje === "User does not exist"? "El usuario no existe":"Contraseña incorrecta",
        cssClass: "alertaEspecial",
        buttons: [
          {
            text: "Ok",
            role: "cancel"
          }
        ]
        });
        alertElement.present();
        console.log(error.error);
        console.log(datos);
        return error.error.datos.resultado;
    });
  }
  loginFaena(id_faena){
    console.log(id_faena);
    this.httpListarFaena()
        .subscribe(data => {
          console.log('Faenas:');
          console.log(data);
          const faenas = [];
          let faenaNombre = '';
          data.datos.resultado.forEach((element, j) => {
            if (element.ID_Faena === id_faena){
              faenas.push(element);
              faenaNombre = element.descripcion;
            }
          });
          // this.faenas = data.datos.resultado;
          // this.faenaService.setFaenas(faenas);
          this.storage.set("idFaena", id_faena);
          this.storage.set("faenanombre", faenaNombre);
          this.navCtrl.navigateRoot(["/inicio"]);
        }, error => {
          console.log("Some error!");
          // this.faenas = error.error.datos.resultado;
        });
  }
  httpListarFaena() {
    this.storage.get('token').then((val) => {
      this.headers.append("x-api-key", val);
    });
    return this.http.get<any>("http://138.99.6.164:8099/ListarFaena", {headers: this.headers});
  }
  httpMySandvik() {
    this.storage.get('token').then((val) => {
      this.headers.append("x-api-key", val);
    });
    return this.http.get<any>("http://138.99.6.164:8099/RutaMySandvik", {headers: this.headers});
  }
  httpGetUsuario(idusuario) {
    this.storage.get('token').then((val) => {
      this.headers.append("x-api-key", val);
    });
    return this.http.get<any>("http://138.99.6.164:8099/ListarUsuario/" + idusuario, {headers: this.headers});
  }
  httpGuardarCambiosUsuario(body) {
    this.storage.get('token').then((val) => {
      this.headers.append("x-api-key", val);
    });
    return this.http.put<any>("http://138.99.6.164:8099/ModificaUsuario/", body, {headers: this.headers});
  }
  httpGetManuales() {
    this.storage.get('token').then((val) => {
      this.headers.append("x-api-key", val);
    });
    return this.http.get<any>("http://138.99.6.164:8099/DocumentosManual", {headers: this.headers});
  }
  httpGetComponentes(sw, fi, ff, faena) {
    this.storage.get('token').then((val) => {
      this.headers.append("x-api-key", val);
    });
    let body = {
      faenaID : faena,
      fechaInicio : fi,
      fechaTermino : ff,
      bitActivo : sw
    };
    return this.http.post<any>("http://138.99.6.164:8099/VistaComponentes", body, {headers: this.headers});
  }
  httpGetComponenteDetalles(componenteid) {
    this.storage.get('token').then((val) => {
      this.headers.append("x-api-key", val);
    });
    let body = {
      idComponente: componenteid
    };
    return this.http.post<any>("http://138.99.6.164:8099/VistaDetalles", body, {headers: this.headers});
  }
  httpGetComponenteDesactivado(componenteid) {
    this.storage.get('token').then((val) => {
      this.headers.append("x-api-key", val);
    });
    let body = {
      idComponente: componenteid
    }  
    return this.http.post<any>("http://138.99.6.164:8099/ComponenteDesactivado", body, {headers: this.headers});
  }
  httpGetDocumentos(idComponente) {
    this.storage.get('token').then((val) => {
      this.headers.append("x-api-key", val);
    });
    let body = {
      idComponente: idComponente
    }  
    return this.http.post<any>("http://138.99.6.164:8099/ObtieneDocumentos", body, {headers: this.headers});
  }
  httpDescargar(ubicacion, nombre) {
    this.storage.get('token').then((val) => {
      this.headers.append("x-api-key", val);
    });
    this.headers.append("Accept", 'application/pdf');
    let body = {
      ruta: ubicacion,
      nombre: nombre
    }
    return this.http.post<any>("http://138.99.6.164:8099/Descarga", body, { headers: this.headers, responseType: 'blob' as 'json' });
  }
}
