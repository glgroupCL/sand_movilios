import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { HttpServiciosService } from '../http-servicios.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import {LoadingController} from '@ionic/angular';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  public faenanombre;
  public administrador = false;

  constructor(public loadingController: LoadingController, public iab: InAppBrowser,private storage: Storage, private router: Router, private activatedRoute: ActivatedRoute, private http: HttpServiciosService,
      public screenOrientation: ScreenOrientation
    ) { }

  ngOnInit() {
    this.storage.get("faenanombre").then(data => {
      this.faenanombre = data;
    });
    this.storage.get("administrador").then(data => {
      this.administrador = data;
    });
    this.storage.get("faenanombre").then(data => {
      this.faenanombre = data;
    });
  }
  ionViewWillEnter() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.storage.get("logged").then(data => {
      if(data != true) {
        this.router.navigate(['/']);
      }
    });
    this.storage.get("faenanombre").then(data => {
      this.faenanombre = data;
    });
    this.storage.set("onfaena", false);
  }
  Salir() {
    this.storage.set("logged", false);
    this.storage.set("token", null);
    this.storage.set("faenanombre", null);

    this.router.navigate(["/"]);
  }
  SeleccionarFaena() {
    this.router.navigate(["/faena"]);
  }
  IrMySandvik() {
    this.loadingController.create({
      message: 'Cargando MySandvik...',
      spinner: 'bubbles'
    }).then((loading) => {
      loading.present();
      this.http.httpMySandvik().subscribe(data => {
        loading.dismiss();
        console.log(data);
        if (data.datos.resultado.length > 0){
          this.iab.create(data.datos.resultado[0].ubicacion, '_system');
        }else{
          this.http.toast('Sin datos');
        }
      }, error => {
        loading.dismiss();
        console.log("Some error!");
        this.http.toast('Verifique su conexion a internet');
      });

    });
  }
  IrUsuario() {
    this.router.navigate(["/usuario"]);
  }
  IrManuales() {
    this.router.navigate(["/manuales"]);
  }
  IrFlotaActiva() {
    this.router.navigate(["/flotaactiva"]);
  }
  IrFlotaInactiva() {
    this.router.navigate(["/flotainactiva"]);
  }
}
