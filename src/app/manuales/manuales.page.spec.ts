import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ManualesPage } from './manuales.page';

describe('ManualesPage', () => {
  let component: ManualesPage;
  let fixture: ComponentFixture<ManualesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ManualesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
