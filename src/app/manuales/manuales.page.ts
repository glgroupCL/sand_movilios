import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { HttpServiciosService } from '../http-servicios.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-manuales',
  templateUrl: './manuales.page.html',
  styleUrls: ['./manuales.page.scss'],
})
export class ManualesPage implements OnInit {

  public manuales = [];
  public manualesSelect = [];
  public componente = "TODOS";

  constructor(public loadingController: LoadingController,public iab: InAppBrowser,public screenOrientation: ScreenOrientation, private http: HttpServiciosService, private storage: Storage, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadingController.create({
      message: "Cargando...",
      spinner: "bubbles"
    }).then((loading) => {
      loading.present();

      this.http.httpGetManuales().subscribe(data => {
        console.log(data);
        this.manuales = data.datos.resultado;
        for(let i = 0; i < this.manuales.length; ++i) {
          let s = "";
          for(let j = 0; j < this.manuales[i].ubicacion.length && j < 36; ++j) {
            s+=this.manuales[i].ubicacion[j];
            if(j == 35) {
              s+="...";
            }
          }
          let sw = false;
          for(let j = 0; j < this.manualesSelect.length; ++j) {
            if(this.manualesSelect[j] == this.manuales[i].descripcion) {
              sw = true;
              break;
            }
          }
          if(!sw) {
            this.manualesSelect.push(this.manuales[i].descripcion);
          }
          this.manuales[i].shortUbicacion = s;
        }
        loading.dismiss();
      })
    });
  }
  ionViewWillEnter() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.storage.get("logged").then(data => {
      if(data != true) {
        this.router.navigate(['/']);
      } 
    })
  }
  IrManual(ubicacion) {
    this.iab.create(ubicacion, '_system');
  }

}
