import { Component, OnInit } from '@angular/core';
import { HttpServiciosService } from '../http-servicios.service';
import { Storage } from '@ionic/storage';
import {AlertController, LoadingController} from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';
import { catchError, map, tap } from 'rxjs/operators';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {

  public usuario: string = "";
  public contrasena: string = "";
  public user = {
    ID_Usuario: null,
    faena_ID: null,
    perfil_ID: null,
    contrasena: null
  }
  public ojo: string = "eye-off-outline";
  public passwordtype: string = "password";
  public passwordtypealert: string = "password";
  public passwordalerttext: string = "Mostrar contraseña";

  constructor(public screenOrientation: ScreenOrientation, public loadingController: LoadingController,
              private http: HttpServiciosService, private storage: Storage, private alertCtrl: AlertController, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }
  async ionViewWillEnter() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.storage.get("logged").then(data => {
      if(data != true) {
        this.router.navigate(['/']);
      } 
    })
    this.storage.get("idusuario").then((idusuario) => {

      this.loadingController.create({
        message: 'Cargando usuario...',
        spinner: 'bubbles'
      }).then((loading) => {
        loading.present();
        this.http.httpGetUsuario(idusuario).subscribe(data => {
          loading.dismiss();
          this.user = data.datos.resultado[0];
        }, error => {
          loading.dismiss();
          console.log('Some error!');
          this.http.toast('Verifique su conexion a internet');
        });

      });
    });
    let alert = await this.alertCtrl.create({
      header: 'Administrar Usuario',
      cssClass: 'alert-naranja-negro',
      mode: "ios",
      subHeader: 'Para modificar los datos del usuario, es necesario ingresar la contraseña actual.',
      inputs: [
        {
          name: 'contrasena',
          placeholder: 'Contraseña',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            this.router.navigate(['/inicio']);
          }
        },
        {
          text: 'Ingresar',
          handler: data => {
            let contrasena = Md5.hashStr(data.contrasena);
            if(data.contrasena === "") {
              this.AlertaMensaje("Escriba la contraseña");
            }
            else if(contrasena != this.user.contrasena) {
              console.log(contrasena);
              console.log(this.user.contrasena);
              this.AlertaMensaje("Contraseña incorrecta");
            }
          }
        }
      ]
    });
    alert.present();
  }
  async AlertaMensaje(mensaje: string) {
    let alertamensaje = await this.alertCtrl.create({
      header: "Error",
      cssClass: 'alert-naranja-negro',
      subHeader: mensaje,
      buttons: [
        {
          text: "Intentar de nuevo",
          role: "cancel",
          handler: () => {
            this.ionViewWillEnter();
          }
        }
      ]
    });
    alertamensaje.present();
  }
  async IrAtras() {
    let alertamensaje = await this.alertCtrl.create({
      header: "Usuario modificado",
      cssClass: 'alert-naranja-negro',
      subHeader: "El usuario ha sido modificado correctamente!",
      buttons: [
        {
          text: "Ir al inicio",
          role: "cancel",
          handler: () => {
            this.router.navigate(["/inicio"]);
          }
        }
      ]
    });
    alertamensaje.present();
  }
  Modificar() {
    let body = {
      idUser: this.user.ID_Usuario,
      nombre: this.usuario,
      contrasena: Md5.hashStr(this.contrasena),
      perfilID: this.user.perfil_ID,
      faena: this.user.faena_ID
    }
    if(body.nombre === "" || this.contrasena === "") {
      this.AlertaMensaje("Por favor, rellene los campos.");
    } else {
      this.loadingController.create({
        message: 'Guardando...',
        spinner: 'bubbles'
      }).then((loading) => {
        loading.present();
        this.http.httpGuardarCambiosUsuario(body).subscribe(data => {
          loading.dismiss();
          this.IrAtras();
        }, error => {
          loading.dismiss();
          console.log('Some error!');
          this.http.toast('Verifique su conexion a internet');
        });

      });
    }
  }
  toggleEye() {
    if(this.ojo == "eye-outline") {
      this.ojo = "eye-off-outline";
      this.passwordtype = "password";
    } else {
      this.ojo = "eye-outline";
      this.passwordtype = "text";
    }
  }  
}
